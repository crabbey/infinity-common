using System.IO;

using Assets.Scripts.Network.Extensions;

namespace Assets.Scripts.Network.Packets
{
    /// <summary>
    /// The LoadLobby (0x3) packet.
    /// </summary>
    public class LoadLobby : InfinityPacket
    {

        public int numShips { get; set; }
         

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadLevel"/> class.
        /// </summary>
        public LoadLobby()
            : base((byte)PacketTypes.LoadLobby)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadLevel"/> class.
        /// </summary>
        /// <param name="br">br</param>
        public LoadLobby(BinaryReader br)
            : base(br)
        {

        }

        public override string ToString()
        {
            return string.Format("[LoadLobby]");
        }

        #region implemented abstract members of InfinityPacket

        public override short GetLength()
        {
            return (short) 1;
        }

        public override void ToStream(Stream stream, bool includeHeader = true)
        {
            /*
             * Length and ID headers get written in the base packet class.
             */
            if (includeHeader)
            {
                base.ToStream(stream, includeHeader);
            }

            /*
             * Always make sure to not close the stream when serializing.
             * 
             * It is up to the caller to decide if the underlying stream
             * gets closed.  If this is a network stream we do not want
             * the regressions of unconditionally closing the TCP socket
             * once the payload of data has been sent to the client.
             */
            using (BinaryWriterLeaveOpen br = new BinaryWriterLeaveOpen(stream, new System.Text.UTF8Encoding()))
            {
            }
        }

        #endregion

    }
}
