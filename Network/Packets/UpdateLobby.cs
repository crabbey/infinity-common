using System.IO;
using UnityEngine;

using Assets.Scripts.Network.Extensions;
using Assets.Scripts.Common.World;

namespace Assets.Scripts.Network.Packets
{
    /// <summary>
    /// The UpdateLobby (0x3) packet.
    /// </summary>
    public class UpdateLobby : InfinityPacket
    {
        public enum actions : byte
        {
            SelectShip = 1,
            SelectRole,
            UpdateShip,
            UpdateRole,
            AddShip,
        }
        public byte action;
        public string test;
        private int length;
        /// <summary>
        /// Initializes a new instance of the <see cref="LoadLevel"/> class.
        /// </summary>
        public UpdateLobby(actions action, params object[] param)
            : base((byte)PacketTypes.LoadLevel)
        {
            this.action = (byte)action;
            switch (this.action)
            {
                case (byte)actions.SelectShip:
                    test = "SelectShip";
                    break;
                case (byte)actions.SelectRole:
                    test = "";
                    break;
                case (byte)actions.UpdateShip:
                    test = "";
                    break;
                case (byte)actions.UpdateRole:
                    test = "";
                    break;
                case (byte)actions.AddShip:
                    var ship = (WorldObject)param[0];
                    test = ship.name;
                    break;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadLevel"/> class.
        /// </summary>
        /// <param name="br">br</param>
        public UpdateLobby(BinaryReader br)
            : base(br)
        {
            action = br.ReadByte();
            switch (action)
            {
                case (byte)actions.SelectShip:
                    test = br.ReadString();
                    break;
                case (byte)actions.SelectRole:
                    test = br.ReadString();
                    break;
                case (byte)actions.UpdateShip:
                    test = br.ReadString();
                    break;
                case (byte)actions.UpdateRole:
                    test = br.ReadString();
                    break;
                case (byte)actions.AddShip:
                    test = br.ReadString();
                    break;
            }
            Debug.Log("[UpdateLobby Recv]: " + test);
        }

        public override string ToString()
        {
            return string.Format("[UpdateLobby]");
        }

        #region implemented abstract members of InfinityPacket

        public override short GetLength()
        {
            length = 1;
            switch (action)
            {
                case (byte)actions.SelectShip:
                    length += (1 + test.Length);
                    break;
                case (byte)actions.SelectRole:
                    length += (1 + test.Length);
                    break;
                case (byte)actions.UpdateShip:
                    length += (1 + test.Length);
                    break;
                case (byte)actions.UpdateRole:
                    length += (1 + test.Length);
                    break;
                case (byte)actions.AddShip:
                    length += (1 + test.Length);
                    break;
            }

            return (short) length;
        }

        public override void ToStream(Stream stream, bool includeHeader = true)
        {
            /*
             * Length and ID headers get written in the base packet class.
             */
            if (includeHeader)
            {
                base.ToStream(stream, includeHeader);
            }

            /*
             * Always make sure to not close the stream when serializing.
             * 
             * It is up to the caller to decide if the underlying stream
             * gets closed.  If this is a network stream we do not want
             * the regressions of unconditionally closing the TCP socket
             * once the payload of data has been sent to the client.
             */
            using (BinaryWriterLeaveOpen br = new BinaryWriterLeaveOpen(stream, new System.Text.UTF8Encoding()))
            {
                br.Write(action);
                switch (action)
                {
                    case (byte)actions.SelectShip:
                        br.Write("SelectShip");
                        break;
                    case (byte)actions.SelectRole:
                        br.Write("SelectRole");
                        break;
                    case (byte)actions.UpdateShip:
                        br.Write("UpdateShip");
                        break;
                    case (byte)actions.UpdateRole:
                        br.Write("UpdateRole");
                        break;
                    case (byte)actions.AddShip:
                        br.Write(test);
                        break;
                }
            }
        }

        #endregion

    }
}
