using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Network.Packets
{
    public enum PlayerStatusTypes : byte
    {
        RequestLobby = 0,
        PerformRespawn,
    }
}
