using System.IO;
using UnityEngine;

using Assets.Scripts.Network.Extensions;
using Assets.Scripts.Common.World;

namespace Assets.Scripts.Network.Packets
{
    /// <summary>
    /// The AddWorldObject (0x7) packet.
    /// </summary>
    public class AddWorldObject : InfinityPacket
    {
        public string test;
        public string EntityName;
        public string EntityPrefabName;
        public string EntityDisplayName;
        public int EntityGlobalID;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadLevel"/> class.
        /// </summary>
        public AddWorldObject(WorldObject obj)
            : base((byte)PacketTypes.AddWorldObject)
        {
            EntityName = obj.name;
            EntityPrefabName = obj.EntityPrefabName;
            EntityGlobalID = obj.EntityGlobalID;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadLevel"/> class.
        /// </summary>
        /// <param name="br">br</param>
        public AddWorldObject(BinaryReader br)
            : base(br)
        {
            EntityName = br.ReadString();
            EntityPrefabName = br.ReadString();
            EntityGlobalID = br.ReadInt32();
        }

        public override string ToString()
        {
            return string.Format("[AddWorldObject]");
        }

        #region implemented abstract members of InfinityPacket

        public override short GetLength()
        {
            var length = 1;
            length += 1 + EntityName.Length;
            length += 1 + EntityPrefabName.Length;
            length += 1; // int entityGlobalID
            return (short) length;
        }

        public override void ToStream(Stream stream, bool includeHeader = true)
        {
            /*
             * Length and ID headers get written in the base packet class.
             */
            if (includeHeader)
            {
                base.ToStream(stream, includeHeader);
            }

            /*
             * Always make sure to not close the stream when serializing.
             * 
             * It is up to the caller to decide if the underlying stream
             * gets closed.  If this is a network stream we do not want
             * the regressions of unconditionally closing the TCP socket
             * once the payload of data has been sent to the client.
             */
            using (BinaryWriterLeaveOpen br = new BinaryWriterLeaveOpen(stream, new System.Text.UTF8Encoding()))
            {
                br.Write(EntityName);
                br.Write(EntityPrefabName);
                br.Write(EntityGlobalID);
            }
        }

        #endregion

    }
}
