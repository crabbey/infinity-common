using System.IO;

using Assets.Scripts.Network.Extensions;

namespace Assets.Scripts.Network.Packets
{
    /// <summary>
    /// The ConnectRequest (0x1) packet.
    /// </summary>
    public class ConnectRequest : InfinityPacket
    {

        public string Username { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectRequest"/> class.
        /// </summary>
        public ConnectRequest(string username)
            : base((byte)PacketTypes.ConnectRequest)
        {
            this.Username = username;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectRequest"/> class.
        /// </summary>
        /// <param name="br">br</param>
        public ConnectRequest(BinaryReader br)
            : base(br)
        {
            this.Username = br.ReadString();
        }

        public override string ToString()
        {
            return string.Format("[ConnectRequest: Username = {0}]", Username);
        }

        #region implemented abstract members of InfinityPacket

        public override short GetLength()
        {
            return (short)(1 + Username.Length);
        }

        public override void ToStream(Stream stream, bool includeHeader = true)
        {
            /*
             * Length and ID headers get written in the base packet class.
             */
            if (includeHeader)
            {
                base.ToStream(stream, includeHeader);
            }

            /*
             * Always make sure to not close the stream when serializing.
             * 
             * It is up to the caller to decide if the underlying stream
             * gets closed.  If this is a network stream we do not want
             * the regressions of unconditionally closing the TCP socket
             * once the payload of data has been sent to the client.
             */
            using (BinaryWriterLeaveOpen br = new BinaryWriterLeaveOpen(stream, new System.Text.UTF8Encoding()))
            {
                br.Write(Username);
            }
        }

        #endregion

    }
}
