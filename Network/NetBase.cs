using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

using Assets.Scripts.Utils;

namespace Assets.Scripts.Network
{
    public partial class NetBase : MonoBehaviour
    {
        public ConnectionHandler ConnectionHandler;
        public DataHandler DataHandler;
        public PacketHandler PacketHandler;

        protected void NetworkReceive()
        {
            if (ConnectionHandler.ConnectionId == -1 && NetworkUtils.IsNetworkClient)
            {
                return;
            }

            int recHostId;
            int connectionId;
            int channelId;
            byte[] recBuffer = new byte[1024];
            int bufferSize = 1024;
            int dataSize;
            byte error;

            NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);

            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Error while receiveing network message: " + (NetworkError)error);
            }

            switch (recData)
            {
                case NetworkEventType.ConnectEvent:
                    {
                        OnConnect(recHostId, connectionId, (NetworkError)error);
                        break;
                    }
                case NetworkEventType.DisconnectEvent:
                    {
                        OnDisconnect(recHostId, connectionId, (NetworkError)error);
                        break;
                    }
                case NetworkEventType.DataEvent:
                    {
                        OnData(recHostId, connectionId, channelId, recBuffer, dataSize, (NetworkError)error);
                        break;
                    }
                case NetworkEventType.BroadcastEvent:
                    {
                        OnBroadcast(recHostId, recBuffer, dataSize, (NetworkError)error);
                        break;
                    }
                case NetworkEventType.Nothing:
                    break;

                default:
                    Debug.LogError("Unknown network message type received: " + recData);
                    break;
            }
        }

        protected virtual void OnConnect(int hostId, int connectionId, NetworkError error)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnDisconnect(int hostId, int connectionId, NetworkError error)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnData(int hostId, int connectionId, int channelId, byte[] data, int size, NetworkError error)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnBroadcast(int hostId, byte[] data, int size, NetworkError error)
        {
            throw new NotImplementedException();
        }
    }
}
