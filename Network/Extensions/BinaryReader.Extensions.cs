﻿using System.IO;

using UnityEngine;

namespace Assets.Scripts.Network.Extensions
{
    public static class BinaryReaderExtensions
    {
        public static Color ReadColor(this BinaryReader br)
        {
            byte[] colourPayload = br.ReadBytes(3);
            return new Color(colourPayload[0], colourPayload[1], colourPayload[2]);
        }
    }
}

