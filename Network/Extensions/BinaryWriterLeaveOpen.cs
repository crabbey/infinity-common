﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Network.Extensions
{
    class BinaryWriterLeaveOpen : BinaryWriter
    {
        public BinaryWriterLeaveOpen(Stream output, Encoding encoding) : base(output, encoding)
        {
        }

        protected override void Dispose(bool disposing)
        {
        }
    }
}
