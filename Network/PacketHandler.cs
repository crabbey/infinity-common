using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{
    public class PacketHandler
    {
        private DataHandler DataHandler;

        public PacketHandler(DataHandler dataHandler)
        {
            this.DataHandler = dataHandler;
        }

        private struct InfinityPacketHeader
        {
            public short length;
            public PacketTypes type;

            public bool IsValid()
            {
                return length > 0 && length < 0x7FFF;
            }
        }

        private InfinityPacketHeader ParseHeader(byte[] buffer, int offset)
        {
            InfinityPacketHeader header = new InfinityPacketHeader();

            header.length = BitConverter.ToInt16(buffer, offset);

            if (Enum.IsDefined(typeof(PacketTypes), buffer[offset + 2]) == false)
            {
                throw new Exception(string.Format("Packet type {0:X2} is unknown", buffer[offset + 2]));
                //header.length = 0;
                //return header;
            }

            header.type = (PacketTypes)buffer[offset + 2];

            return header;
        }

        public void HandlePacket (byte[] data, int connectionId)
        {
            InfinityPacketHeader packetHeader = ParseHeader(data, 0);
            if (packetHeader.IsValid() == false)
            {
                Console.WriteLine("Packet is invalid");
            }
            Console.WriteLine(string.Format("Received packet {0} from {1}", packetHeader.type, connectionId));

            using (MemoryStream ms = new MemoryStream(data))
            using (BinaryReader br = new BinaryReader(ms))
            {
                InfinityPacket packet = InfinityPacket.Deserialize(br);
                DataHandler.HandleData(packetHeader.type, packet, connectionId);
            }
        }
    }
}
