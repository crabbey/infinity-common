using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{
    [Serializable]
    class ClientConnectionHandler : ConnectionHandler
    {

        public override void InitNetwork()
        {
            NetworkTransport.Init();
            NetworkChannels = new Dictionary<NetworkChannel, byte>();
            ConnectionConfig config = new ConnectionConfig
            {
                MaxConnectionAttempt = 3,
                ConnectTimeout = 5000
            };

            NetworkChannels.Add(NetworkChannel.Unreliable, config.AddChannel(QosType.Unreliable));
            NetworkChannels.Add(NetworkChannel.Reliable, config.AddChannel(QosType.Reliable));
            NetworkChannels.Add(NetworkChannel.ReliableFragmented, config.AddChannel(QosType.ReliableFragmented));
            NetworkChannels.Add(NetworkChannel.ReliableSequenced, config.AddChannel(QosType.ReliableSequenced));
            NetworkChannels.Add(NetworkChannel.StateUpdate, config.AddChannel(QosType.StateUpdate));
            NetworkChannels.Add(NetworkChannel.AllCostDelivery, config.AddChannel(QosType.AllCostDelivery));

            HostTopology topology = new HostTopology(config, 10);

            HostId = NetworkTransport.AddHost(topology);
        }

        public void Connect(string ipaddress, int port)
        {
            Debug.Log("Connecting to " + ipaddress + ":" + port);

            byte error;
            ConnectionId = NetworkTransport.Connect(HostId, ipaddress, port, 0, out error);
            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Failed to connect: " + (NetworkError)error);
            }
        }

        public void Disconnect()
        {
            byte error;
            NetworkTransport.Disconnect(HostId, ConnectionId, out error);
        }

        public override void SendData(InfinityPacket packet, NetworkChannel channel, int connectionId)
        {
            byte[] buf = packet.ToArray();

            byte channelId;
            if (!NetworkChannels.TryGetValue(channel, out channelId))
            {
                Debug.LogError($"Invalid channel: { channel.ToString() }");
                return;
            }

            byte error;
            NetworkTransport.Send(HostId, connectionId, channelId, buf, buf.Length, out error);

            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Error while sending network message: " + (NetworkError)error);
                return;
            }

            Debug.Log(string.Format("Sent packet {0} to {1}", packet.PacketType, connectionId));

        }

        public override void SendData(InfinityPacket packet, NetworkChannel channel)
        {
            SendData(packet, channel, 1);
        }

        public override void Disconnect(int connectionId)
        {
            byte error;
            NetworkTransport.Disconnect(HostId, connectionId, out error);
            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError($"Failed to disconnect { connectionId }, error: { error }");
            }
        }
    }
}
