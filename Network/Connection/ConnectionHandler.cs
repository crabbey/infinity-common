using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{
    [Serializable]
    public enum NetworkChannel : byte
    {
        Unreliable = 1,
        Reliable,
        ReliableFragmented,
        ReliableSequenced,
        StateUpdate,
        AllCostDelivery
    }

    [Serializable]
    public partial class ConnectionHandler
    {
        public int HostId = -1;
        public int ConnectionId = -1;
        public List<int> ConnectionList;

        public Dictionary<NetworkChannel, byte> NetworkChannels;

        public virtual void InitNetwork()
        {
            throw new NotImplementedException();
        }

        public virtual void SendData(InfinityPacket packet, NetworkChannel channel, int connectionId)
        {
            throw new NotImplementedException();
        }

        public virtual void SendData(InfinityPacket packet, NetworkChannel channel)
        {
            throw new NotImplementedException();
        }

        public virtual void Disconnect(int connectionId)
        {
            throw new NotImplementedException();
        }
    }
}
