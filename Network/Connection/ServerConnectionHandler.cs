using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

using Assets.Scripts.Utils;

namespace Assets.Scripts.Network
{
    [Serializable]
    class ServerConnectionHandler : ConnectionHandler
    {
        public override void InitNetwork()
        {
            NetworkTransport.Init();
            NetworkChannels = new Dictionary<NetworkChannel, byte>();
            ConnectionConfig config = new ConnectionConfig
            {
                MaxConnectionAttempt = 3,
                ConnectTimeout = 5000
            };

            NetworkChannels.Add(NetworkChannel.Unreliable, config.AddChannel(QosType.Unreliable));
            NetworkChannels.Add(NetworkChannel.Reliable, config.AddChannel(QosType.Reliable));
            NetworkChannels.Add(NetworkChannel.ReliableFragmented, config.AddChannel(QosType.ReliableFragmented));
            NetworkChannels.Add(NetworkChannel.ReliableSequenced, config.AddChannel(QosType.ReliableSequenced));
            NetworkChannels.Add(NetworkChannel.StateUpdate, config.AddChannel(QosType.StateUpdate));
            NetworkChannels.Add(NetworkChannel.AllCostDelivery, config.AddChannel(QosType.AllCostDelivery));

            HostTopology topology = new HostTopology(config, 10);

            HostId = NetworkTransport.AddHost(topology, NetServer.Port, NetServer.IPAddress);
            ConnectionList = new List<int>();
            Debug.Log($"[Server] Opened network socket on { NetServer.IPAddress }:{ NetServer.Port }");
        }

        public virtual void Connect(string ipaddress, int port)
        {
            Debug.Log("Connecting to " + ipaddress + ":" + port);

            byte error;
            ConnectionId = NetworkTransport.Connect(HostId, ipaddress, port, 0, out error);
            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Failed to connect: " + (NetworkError)error);
            }
        }

        public override void SendData(InfinityPacket packet, NetworkChannel channel, int connectionId)
        {
            byte[] buf = packet.ToArray();

            byte channelId;
            if (!NetworkChannels.TryGetValue(channel, out channelId))
            {
                Debug.LogError($"Invalid channel: { channel.ToString() }");
                return;
            }

            byte error;
            NetworkTransport.Send(HostId, connectionId, channelId, buf, buf.Length, out error);

            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError("Error while sending network message: " + (NetworkError)error);
                return;
            }

            Debug.Log($"Sent packet { packet.ToString() } to { connectionId }");
        }

        public override void Disconnect(int connectionId)
        {
            byte error;
            NetworkTransport.Disconnect(HostId, connectionId, out error);
            if ((NetworkError)error != NetworkError.Ok)
            {
                Debug.LogError($"Failed to disconnect { connectionId }, error: { error }");
            }
        }

        public void BroadcastData(InfinityPacket packet, NetworkChannel channel)
        {
            byte[] buf = packet.ToArray();

            byte channelId;
            if (!NetworkChannels.TryGetValue(channel, out channelId))
            {
                Debug.LogError($"Invalid channel: { channel.ToString() }");
                return;
            }

            Parallel.ForEach(ConnectionList, connectionId =>
            {
                byte error;
                NetworkTransport.Send(HostId, connectionId, channelId, buf, buf.Length, out error);

                if ((NetworkError)error != NetworkError.Ok)
                {
                    Debug.LogError("Error while sending network message: " + (NetworkError)error);
                    return;
                }
            });

            Debug.Log($"Broadcast packet { packet.ToString() } to { ConnectionList.Count } connections");
        }
    }
}
