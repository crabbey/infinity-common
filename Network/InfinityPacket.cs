using System;
using System.Collections.Generic;
using System.IO;

using Assets.Scripts.Network.Extensions;
using Assets.Scripts.Network.Packets;

namespace Assets.Scripts.Network
{
	/// <summary>
	/// Abstract base class generically representing a infinity packet.
	/// </summary>
	public abstract class InfinityPacket
	{
		public const short PACKET_HEADER_LEN = 3;
		public byte[] TestRawBuffer { get; set; }

		protected short _length = 0;

		/// <summary>
		/// The deserializer map.
		/// 
		/// Deserializer maps point to a function to return a fully qualified packet
		/// from one supplied BinaryReader object.  Derivatives of InfinityPacket
		/// should make sure that they return a valid packet structure when passed a
		/// BinaryReader to deserialize from.
		/// </summary>
		public static Dictionary<PacketTypes, Func<BinaryReader, InfinityPacket>> deserializerMap = new Dictionary<PacketTypes, Func<BinaryReader, InfinityPacket>>() {
			/*001*/ { PacketTypes.ConnectRequest, (br) => new ConnectRequest(br) },
			/*002*/ { PacketTypes.Disconnect, (br) => new Disconnect(br) },
            /*003*/ { PacketTypes.LoadLevel, (br) => new LoadLevel(br) },
            /*004*/ { PacketTypes.PlayerStatus, (br) => new PlayerStatus(br) },
            /*005*/ { PacketTypes.LoadLobby, (br) => new LoadLobby(br) }, /* Possibly duplicate of RequestLobby */
            /*006*/ { PacketTypes.UpdateLobby, (br) => new UpdateLobby(br) },
            /*007*/ { PacketTypes.AddWorldObject, (br) => new AddWorldObject(br) },
        };

		/// <summary>
		/// Gets the packet length in bytes.
		/// </summary>
		public abstract short GetLength();

		/// <summary>
		/// Gets or sets the Packet ID.
		/// </summary>
		public byte ID { get; protected set; }

		/// <summary>
		/// Gets the type of the packet.
		/// </summary>
		public PacketTypes PacketType
		{
			get
			{
				return (PacketTypes)this.ID;
			}
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InfinityPacket"/> with 
        /// the specified BinaryReader object to deserialize a derivative on.
        /// </summary>
        /// <param name="br">
        /// A reference to a BinaryReader which contains binary payload to be deserialized into
        /// a fully-qualified InfinityPacket.
        /// </param>
        protected InfinityPacket(BinaryReader br)
		{
			_length = br.ReadInt16();
			this.ID = br.ReadByte();
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="InfinityPacket"/> class.
        /// </summary>
        /// <param name="id">Identifier.</param>
        protected InfinityPacket(byte id)
		{
			this.ID = id;
		}

		/// <summary>
		/// Deserializes a packet from the specified binary reader and returns a InfinityPacket 
		/// derivative according to the deserializer methods in deserializerMap.
		/// </summary>
		/// <param name="br">
		/// An instance of a BinaryReader which contains a binary infinity packet payload in 
		/// which to deserialize an object from
		/// </param>
		/// <param name="id">
		/// Packet identifier that is used to find the deserializer method via deserializerMap
		/// </param>
		public static InfinityPacket Deserialize(BinaryReader br, byte id)
		{
			br.BaseStream.Seek(0, SeekOrigin.Begin);

			if (deserializerMap.ContainsKey((PacketTypes)id) == false)
			{
				return new UnknownPacket(br);
			}

			return deserializerMap[(PacketTypes)id](br);
		}

		/// <summary>
		/// Deserializes a packet from the specified binary reader and returns a InfinityPacket 
		/// derivative according to the deserializer methods in deserializerMap.
		/// </summary>
		/// <param name="br">
		/// An instance of a BinaryReader which contains a binary infinity packet payload in 
		/// which to deserialize an object from
		/// </param>
		public static InfinityPacket Deserialize(BinaryReader br)
		{
			br.ReadInt16();
			byte id = br.ReadByte();

			return Deserialize(br, id);
		}

		/// <summary>
		/// Serializes this InfinityPacket instance into the provided stream.
		/// </summary>
		/// <param name="stream">
		/// A reference to a valid, open, and writable stream object in which to serialize this
		/// instance to.
		/// </param>
		public virtual void ToStream(Stream stream, bool includeHeader = true)
		{
			if (includeHeader == false)
			{
				return;
			}

			using (BinaryWriterLeaveOpen br = new BinaryWriterLeaveOpen(stream, System.Text.Encoding.UTF8))
			{
				br.Write((short)(GetLength() + PACKET_HEADER_LEN));
				br.Write(ID);
			}
		}

		/// <summary>
		/// Returns a byte array with the binary contents of this InfinityPacket instance.
		/// </summary>
		public virtual byte[] ToArray(bool includeHeader = true)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				ToStream(ms, includeHeader);
				return ms.ToArray();
			}
		}
	}
}

