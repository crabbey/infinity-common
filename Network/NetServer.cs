using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Assets.Scripts.Network.Events;
using Assets.Scripts.Network;
using Assets.Scripts.World;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts.Network
{

    class NetServer : NetBase
    {
        public static string IPAddress = "0.0.0.0";
        public static int Port = 6606;
        public static Universe universe;

        void Start()
        {
            ConnectionHandler = new ServerConnectionHandler(); // Handles raw sockets
            DataHandler = new DataHandler(); // Handles the packet data and fires events
            PacketHandler = new PacketHandler(DataHandler); // Handles the raw byte stream and parses into readable data

            ConnectionHandler.InitNetwork();
            
            DataHandler.ConnectRequest.AddListener(OnConnectRequest);
            if (!universe)
            {
                universe = Instantiate(Resources.Load<Universe>("Prefabs/Universe"));
                universe.name = "Universe";
            }
        }

        void Update()
        {
            NetworkReceive();
        }

        protected override void OnConnect(int hostId, int connectionId, NetworkError error)
        {
            Debug.Log("[Server] OnConnect(hostId = " + hostId + ", connectionId = "
                + connectionId + ", error = " + error.ToString() + ")");
            (ConnectionHandler as ServerConnectionHandler).ConnectionList.Add(connectionId);
            DataHandler.HandleConnect(connectionId, error);
        }

        protected override void OnDisconnect(int hostId, int connectionId, NetworkError error)
        {
            Debug.Log("[Server] OnDisconnect(hostId = " + hostId + ", connectionId = "
                + connectionId + ", error = " + error.ToString() + ")");
            (ConnectionHandler as ServerConnectionHandler).ConnectionList.Remove(connectionId);
            DataHandler.HandleDisconnect(connectionId, error);
        }

        protected override void OnBroadcast(int hostId, byte[] data, int size, NetworkError error)
        {
            Debug.Log("[Server] OnBroadcast(hostId = " + hostId + ", data = "
                + data + ", size = " + size + ", error = " + error.ToString() + ")");
        }

        protected override void OnData(int hostId, int connectionId, int channelId, byte[] data, int size, NetworkError error)
        {
            PacketHandler.HandlePacket(data, connectionId);
        }

        void OnConnectRequest(PacketEventArgs args)
        {
            Debug.Log("Received OnConnectRequest from connectionId: " + args.Sender);
            ConnectionHandler.SendData(new Packets.LoadLevel(), NetworkChannel.AllCostDelivery, args.Sender);
        }
    }
}
