namespace Assets.Scripts.Network
{
	public enum PacketTypes : byte
	{
		/*001*/ ConnectRequest = 1,
		/*002*/ Disconnect,
        /*003*/ LoadLevel,
        /*004*/ PlayerStatus,
        /*005*/ LoadLobby,
        /*006*/ UpdateLobby,
        /*007*/ AddWorldObject,
    }
}

