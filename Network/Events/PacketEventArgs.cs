﻿using System;

namespace Assets.Scripts.Network.Events
{
	/// <summary>
	/// Packet event arguments.
	/// </summary>
	public class PacketEventArgs : EventArgs
	{
        /// <summary>
        /// Gets the packet received by the client.
        /// </summary>
        public InfinityPacket Packet { get; set; }

        /// <summary>
        /// Gets the connectionId of the sender
        /// </summary>
        public int Sender { get; set; }

        public PacketEventArgs(InfinityPacket packet, int sender)
        {
            this.Packet = packet;
            this.Sender = sender;
        }
	}
}

