using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Assets.Scripts.Network.Events;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;


namespace Assets.Scripts.Network
{
    [System.Serializable]
    public class NetworkDataEvent : UnityEvent<PacketEventArgs>
    {
    }

    [System.Serializable]
    public class NetworkEvent : UnityEvent<int>
    {
    }

    public class DataHandler
    {
        public NetworkEvent NetworkConnect = new NetworkEvent();
        public NetworkEvent NetworkDisconnect = new NetworkEvent();

        public NetworkDataEvent ConnectRequest = new NetworkDataEvent();
        public NetworkDataEvent Disconnect = new NetworkDataEvent();
        public NetworkDataEvent LoadLevel = new NetworkDataEvent();
        public NetworkDataEvent PlayerStatus = new NetworkDataEvent();
        public NetworkDataEvent LoadLobby = new NetworkDataEvent();
        public NetworkDataEvent UpdateLobby = new NetworkDataEvent();
        public NetworkDataEvent AddWorldObject = new NetworkDataEvent();

        private Dictionary<PacketTypes, NetworkDataEvent> DataHandlerEvents;

        public DataHandler()
        {
            DataHandlerEvents = new Dictionary<PacketTypes, NetworkDataEvent>
            {
                { PacketTypes.ConnectRequest, ConnectRequest },
                { PacketTypes.Disconnect, Disconnect },
                { PacketTypes.LoadLevel, LoadLevel },
                { PacketTypes.PlayerStatus, PlayerStatus },
                { PacketTypes.LoadLobby, LoadLobby },
                { PacketTypes.UpdateLobby, UpdateLobby },
                { PacketTypes.AddWorldObject, AddWorldObject},
            };
        }

        public void HandleData(PacketTypes type, InfinityPacket packet, int connectionId)
        {
            NetworkDataEvent handler;
            if (DataHandlerEvents.TryGetValue(type, out handler))
            {
                try
                {
                    handler.Invoke(new PacketEventArgs(packet, connectionId));
                }
                catch (Exception ex)
                {
                    Debug.LogException(ex);
                }
            }
        }

        public void HandleConnect(int connectionId, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                Debug.LogError($"Received error on connect event { error.ToString() }");
                return;
            }
            NetworkConnect.Invoke(connectionId);
        }

        public void HandleDisconnect(int connectionId, NetworkError error)
        {
            NetworkDisconnect.Invoke(connectionId);
        }
    }
}
