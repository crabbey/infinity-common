﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Network.Packets;

namespace Assets.Scripts.Common.World
{
    public class WorldObject : MonoBehaviour
    {
        public int EntityGlobalID; // The ID used in the universe dictionary to identify this object
        public string EntityPrefabName;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public virtual void InstanciateFromPacket(AddWorldObject wo)
        {
            this.EntityPrefabName = wo.EntityPrefabName;
            this.EntityGlobalID = wo.EntityGlobalID;

        }
    }
}