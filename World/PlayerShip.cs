﻿using UnityEngine;
using System.Collections;


namespace Assets.Scripts.Common.World
{
    public class PlayerShip : WorldObject
    {
        public string Shipname;


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        string toString()
        {
            return "PlayerShip Object named" + Shipname;
        }
    }
}