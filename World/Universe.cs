﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.Utils;
using Assets.Scripts.Network;
using Assets.Scripts.Network.Events;
using Assets.Scripts.Network.Packets;

namespace Assets.Scripts.Common.World
{
    public class Universe : MonoBehaviour
    {
        public Dictionary<int, WorldObject> worldObjects = new Dictionary<int, WorldObject>();
        internal DataHandler DataHandler;
        internal GameObject WorldEntites;

        // Use this for initialization
        public virtual void Start()
        {
            DataHandler = NetworkUtils.FindDataHandler();
            DataHandler.AddWorldObject.AddListener(onAddWorldObject);
            WorldEntites = gameObject.transform.Find("WorldEntities").gameObject;
        }

        // Update is called once per frame
        public virtual void Update()
        {

        }

        public virtual void onAddWorldObject(PacketEventArgs args)
        {

        }
    }
}